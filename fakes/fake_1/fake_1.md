# Fake 1

I call this Bluepill "fake_1".

## Visual clues

Front: It has a round, metal RESET button with octo-shaped housing, also in metal. The text "RESET" goes vertically down to pin A11 on the right side of the RESET button. There is  text "0" to the left and text "1" to the right of the BOOT0 / BOOT1 jumpers - no text to indicate BOOT0 / BOOT1. The microusb connector has two support legs on each side, none of these are soldered.

Back: the microusb connector fits in a square cutout in the pcb. The text "HW" is printed along one side (outside of the pin headers, if soldered
on the back side) at the end with the "G" and "G". On the short edge with the four-pin connector, the text "HW-267" is printed along the edge.

STM32 chip: the STM32 chip has three depression ("dots") in the top plastic casing. The text on the chip is:
```
STM32
F103C8T6
991KA 93
MYS 807
```
with some symbols below (ST logo, ? , ?)

## Programming it
(set BOOT0 jumper to '1' first) check
```
$ stm32flash /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
```
program blinky
```
$ stm32flash -w ../Greaseweazle-v0.27/hex/alt/Blinky_Test-F1-v0.27.hex  /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Intel HEX
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x08001064 (100.00%) Done.
```
reset BOOT0 jumper and check via `screen /dev/ttyUSB1 115200` (press RESET if needed)
```
** Blinky Test **
** Keir Fraser <keir.xen@gmail.com>
** https://github.com/keirf/Greaseweazle
Serial = 78dc:046f:1d2d:1800:07e2:1607
Flash Size  = 64kB
Device ID = 0x0410
Revision  = 0x2003
**WARNING**: 10xx8/B device returned valid IDCODE! Fake?
Testing I2C1... [Fake Chip?] **FAILED**
Testing I2C2... [Fake Chip?] **FAILED**
Testing SPI1... OK
Testing SPI2... OK
Testing TIM1... OK
Testing TIM2... OK
Testing TIM3... OK
Testing TIM4... OK
DMA Test #1... [Spurious IRQ: Fake Chip?] **FAILED**
DMA Test #2... [Spurious IRQ: Fake Chip?] [Bad Data] **FAILED**
DMA Test #3... [Spurious IRQ: Fake Chip?] [Bad Data] **FAILED**
DMA Test #4... [Spurious IRQ: Fake Chip?] [Bad Data] **FAILED**
Testing 64kB Flash... OK
Enable TIM4 IRQ... OK
Testing 20kB SRAM (endless loop)...
```
and here it hangs

flash Bluepill diagnostics
```
$ stm32flash -w ../id-swdcom/bluepill-diagnostics-v1.631.bin /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x08003300 (20.16%) Failed to read ACK byte
Failed to write memory at address 0x08003300
```
(I had to try a few times before it succeded)
```
$ stm32flash -w ../id-swdcom/bluepill-diagnostics-v1.631.bin /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x0800fd00 (100.00%) Done.
```
but unfortunately, after resetting BOOT0 jumper and connecting via usb, no usb serial port shows up. So I program another one.
```
$ stm32flash -w ../id-swdcom/bluepill-diagnostics-v1.631.bin /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x0800fd00 (100.00%) Done.
```
much better. Now, will it run the test? Nope, the serial port never shows up. I get this in /var/log/messages
```
May 31 19:30:09 kg-bsbox kernel: [5293612.325234] usb 1-1: new full-speed USB device number 55 using xhci_hcd
May 31 19:30:09 kg-bsbox kernel: [5293612.609208] usb 1-1: new full-speed USB device number 56 using xhci_hcd
May 31 19:30:09 kg-bsbox kernel: [5293612.761877] usb usb1-port1: attempt power cycle
May 31 19:30:10 kg-bsbox kernel: [5293613.417218] usb 1-1: new full-speed USB device number 57 using xhci_hcd
May 31 19:30:10 kg-bsbox kernel: [5293613.439029] usb 1-1: config index 0 descriptor too short (expected 67, got 64)
May 31 19:30:10 kg-bsbox kernel: [5293613.439037] usb 1-1: config 1 has an invalid descriptor of length 7, skipping remainder of the config
May 31 19:30:10 kg-bsbox kernel: [5293613.439051] usb 1-1: config 1 interface 1 altsetting 0 has 1 endpoint descriptor, different from the interface descriptor's value: 2
May 31 19:30:10 kg-bsbox kernel: [5293613.439601] usb 1-1: string descriptor 0 read error: -75
May 31 19:30:10 kg-bsbox kernel: [5293613.439612] usb 1-1: New USB device found, idVendor=0483, idProduct=5740, bcdDevice= 2.00
May 31 19:30:10 kg-bsbox kernel: [5293613.439616] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
May 31 19:30:10 kg-bsbox kernel: [5293613.441537] cdc_acm: probe of 1-1:1.0 failed with error -22
May 31 19:30:10 kg-bsbox mtp-probe: checking bus 1, device 57: "/sys/devices/pci0000:00/0000:00:14.0/usb1/1-1"
May 31 19:30:10 kg-bsbox mtp-probe: bus: 1, device: 57 was not an MTP device
May 31 19:30:10 kg-bsbox mtp-probe: checking bus 1, device 57: "/sys/devices/pci0000:00/0000:00:14.0/usb1/1-1"
May 31 19:30:10 kg-bsbox mtp-probe: bus: 1, device: 57 was not an MTP device
```
testing ends here.
