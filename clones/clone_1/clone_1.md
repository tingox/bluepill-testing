# Clone_1

I call this Bluepill "clone_1"

## Visual clues

Front: It has the text "STM32*" just above A10-A12 pins. It has a rectangular RESET button, the button and the housing is white-ish plastic, the
top cover is metal. To the right of the button is the text "RESRT". Above the RESET button and below the jumpers is the text "BOOT1", above the jumpers is the text "BOOT0". The jumpers have the text "0" on the left and "1" on the right side.

Back: no specific visual clues.

Other: there is a hole in the PCB between the "G" and "G" pins and text in the upper left corner seen from the front.

STM32 chip: the STM32 chip has only one depression ("dot" in the top plastic casing. The text on the chip is:
```
STM32F
103C8T6
9910X RR93
MYS 99 907
```
and with some symbols below (ST logo, ? , ?)

## Programming it
(set BOOT0 jumper to '1' first) check
```
$ stm32flash /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
```

program blinky
```
$ stm32flash -w ../Greaseweazle-v0.27/hex/alt/Blinky_Test-F1-v0.27.hex  /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Intel HEX
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x08001064 (100.00%) Done.
```
reset BOOT0 jumper and check via `screen /dev/ttyUSB1 115200` (press RESET if needed)
```
** Blinky Test **
** Keir Fraser <keir.xen@gmail.com>
** https://github.com/keirf/Greaseweazle
Serial = 2a03:0105:122a:4d38:4b43:004e
Flash Size  = 128kB
Device ID = 0x0410
Revision  = 0x2003
**WARNING**: 10xx8/B device returned valid IDCODE! Fake?
Testing I2C1... OK
Testing I2C2... OK
Testing SPI1... OK
Testing SPI2... OK
Testing TIM1... OK
Testing TIM2... OK
Testing TIM3... OK
Testing TIM4... OK
DMA Test #1... OK
DMA Test #2... OK
DMA Test #3... OK
DMA Test #4... OK
Testing 64kB Flash... OK
Enable TIM4 IRQ... .OK
Testing 20kB SRAM (endless loop)..
```
Next, flash Bluepill Diagnostics
```
$ stm32flash -w ../id-swdcom/bluepill-diagnostics-v1.631.bin /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x0800fd00 (100.00%) Done.
```
(reset BOOT0 jumper) connect via usb, do `screen /dev/ttyACM0 460800` then press 'm'
```
--------- 
Main Menu 
--------- 
h - test second Half of the 128KB flash declared for this chip
f - how much Flash is declared in the Flash Size Register ? 
d - Print DBGMCU_IDCODE 
a - STM32F103C8T6 Authenticity test, don't use with SWD/JTAG. Requires test h once to complete
j - Jdec manufacturer id 
e - Extra menu 
q - Quit menu, enter the Forth command line 
m - Main menu 
```
The tests
```
'h' test - "Please wait, testing Flash ......" and returns to main menu
'f' test - "131072 flash is declared in the Flash size register at 0x1FFFF7E0"
'd' test - "DBGMCU_IDCODE [@ 0xE0042000] = 0x20036410"
'a' test -
STM32F103C8 authentication FAILED one or more tests: 
---------------------------------------------------- 
FAIL - Declared flash not 65536 
FAIL - DBGMCU_IDCODE is readable with no SWD/Jtag connected 
PASS - Second 64KB flash block verified 
FAIL - JDEC manufacturer id NOT STMicroelectronics 
'j' test - 
Jdec Continuation Code: 0x04
Jdec Identity Code: 0x3B
JDEC manufacturer id: CKS or APM 
extra menu
'i' test -
UNIQUE DEVICE ID: 
----------------- 
BITS-95:64 | 0x004E4B43 | .NKC
BITS-63:32 | 0x4D38122A | M8.*
BITS-31:0  | 0x01052A03 | ..*.
'n' test - "Unique Serial Number = 0x4C73736A"
```
and that was it.
