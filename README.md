# Bluepill Testing

Just some notes on testing various Bluepill boards.

clones - contains samples of bluepill boards that fails testing in some way, but still works reliably in a real application.

- [clone_1](./clones/clone_1/clone_1.md) - I have quite a few of these, they work well.

fakes - contains samples of bluepill boards that totally fails testing, or they are unreliable in operation.
- [fake_1](./fakes/fake_1/fake_1.md) - failed most of the tests, and unreliable in operation.

real - contains samples of Bluepill boards that passes all tests
- [real_1](./real/real_1/real_1.md) - this one passes all tests.
