# Real 1

I call this Bluepill "real_1".

## Visual clues

Front: It has a round, metal RESET button with octo-shaped housing, also in metal. The text "RESET" goes vertically down to pin A11 on the right side of the RESET button. There is  text "0" to the left and text "1" to the right of the BOOT0 / BOOT1 jumpers - no text to indicate BOOT0 / BOOT1. The microusb connector has two support legs on each side, none of these are soldered.

Back: the microusb connector fits in a square cutout in the pcb.

STM32 chip: it has only one depression ("dot") in the top plastic housing. The text on the chip is:
```
STM32
F103C8T6
991DG 9U
MYS 424
```
ST logo, a round symbol and the number "2" below.

## Programming it
(set BOOT0 jumper to '1' first)

program blinky
```
$ stm32flash -w ../Greaseweazle-v0.27/hex/alt/Blinky_Test-F1-v0.27.hex  /dev/ttyUSB1
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Intel HEX
Interface serial_posix: 57600 8E1
Version      : 0x22
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0410 (STM32F10xxx Medium-density)
- RAM        : 20KiB  (512b reserved by bootloader)
- Flash      : 128KiB (size first sector: 4x1024)
- Option RAM : 16b
- System RAM : 2KiB
Write to memory
Erasing memory
Wrote address 0x08001064 (100.00%) Done.
```
reset BOOT0 jumper and check via `screen /dev/ttyUSB1 115200` (press RESET if needed)
```
** Blinky Test **
** Keir Fraser <keir.xen@gmail.com>
** https://github.com/keirf/Greaseweazle
Serial = ff50:066e:8350:5152:1814:8720
Flash Size  = 64kB
Device ID = 0x0000
Revision  = 0x0000
Testing I2C1... OK
Testing I2C2... OK
Testing SPI1... OK
Testing SPI2... OK
Testing TIM1... OK
Testing TIM2... OK
Testing TIM3... OK
Testing TIM4... OK
DMA Test #1... OK
DMA Test #2... OK
DMA Test #3... OK
DMA Test #4... OK
Testing 64kB Flash... OK
Enable TIM4 IRQ... .OK
Testing 20kB SRAM (endless loop)..................
```

test Bluepill Diagnostics, connect via usb, do `screen /dev/ttyACM0 460800`, press 'm'
```
--------- 
Main Menu 
--------- 
h - test for Hidden second 64kb flash block: 0x10000 - 0x1FFFF
f - how much Flash is declared in the Flash Size Register ? 
d - Print DBGMCU_IDCODE 
a - STM32F103C8T6 Authenticity test, don't use with SWD/JTAG. Requires test h once to complete
j - Jdec manufacturer id 
e - Extra menu 
q - Quit menu, enter the Forth command line 
m - Main menu 
```
tests
```
'h' test - test for Hidden second 64kb flash block: 0x10000 - 0x1FFFF:  PASSED
'f' test - 65536 flash is declared in the Flash size register at 0x1FFFF7E0
'd' test - DBGMCU_IDCODE [@ 0xE0042000] = 0x00000000
STM32F103C8 authentication PASSED all these tests:
---------------------------------------------------- 
PASS - Declared flash = 65536 Bytes 
PASS - DBGMCU_IDCODE is NOT readable without SWD/Jtag connected 
PASS - Second 64KB flash block verified 
PASS - JDEC manufacturer id IS STMicroelectronics 
'j' test - 
Jdec Continuation Code: 0x00
Jdec Identity Code: 0x20
JDEC manufacturer id: STMicroelectronics 
extra menu
'i' test -
UNIQUE DEVICE ID: 
----------------- 
BITS-95:64 | 0x87201814 | . ..
BITS-63:32 | 0x51528350 | QR.P
BITS-31:0  | 0x066EFF50 | .n.P
'n' test - 
Unique Serial Number = 0xD01C6414 
```
that's all.

